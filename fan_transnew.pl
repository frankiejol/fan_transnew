#!/usr/bin/perl

use warnings;
use strict;
use Carp qw(confess);
use File::Copy;
use Getopt::Long;
use IPC::Run qw(run);
use Proc::PID::File;

my $DEBUG;
my $VERBOSE = $ENV{TERM};

my @pid_args = ();
@pid_args = ( dir => "/var/tmp/")    if $>;
if ( Proc::PID::File->running(@pid_args) ) {
    warn "Already running\n" if $DEBUG || $VERBOSE;
    exit;
}

######################################################################
# default args
my $VERSION = '0.01';
my $SECONDS_OLD = 10;
my $AVCONV = "/usr/bin/avconv";
$AVCONV = "/usr/local/bin/avconv"   if !-e $AVCONV;
my $DEFAULT_EXT= 'mp4';

######################################################

my %VALID_EXT = map { $_ => 1 } qw( avi flv mkv mov mp4 wmv );
my %OPTS = (
     mp4 => '-c:v libx264 -strict experimental -c:a aac -ac 2 -ab 160k -maxrate 10000000 -bufsize 10000000 '
    ,webm => '-c:v vp8 -strict experimental -c:a vorbis -ac 2'
);
my %FILES_SRC;
my %PROCESSED;
######################################################################
#
# init
#
my ($me) = $0 =~ m{.*/(.*)};
$me = $0 if !$me;
my $USAGE .="$me [--help] [--version] [--debug] [--verbose]"
    ." [--seconds-old=$SECONDS_OLD]"
    ." [--avconv=$AVCONV]"
#    ." [--avconv-opts=\"--bla --ble\"]"
    ." dir\n"
    ."  --seconds-old : Minim número de segons d'antiguitat de l'arxiu per procesar.\n"
    ;

my ($help,$version);
GetOptions(
                 help => \$help
               ,debug => \$DEBUG
             ,version => \$version
             ,verbose => \$VERBOSE
           ,'avconv=s'=> \$AVCONV
#     ,'avconv-opts=s' => \$AVCONV_OPTS
     ,'seconds-old=s' => \$SECONDS_OLD
) or exit;
if ($version) {
    print "$me v$VERSION\n";
    exit;
}
if ($help) {
    print $USAGE;
    exit;
}
my $DIR = $ARGV[0] or die "ERROR: Falta dir\n $USAGE";

die "Missing $AVCONV\n" if ! -e $AVCONV;
#######################################################################
#
# subs
#

sub avconv_opts {
    my $opts = shift or return;

    my @opts = split /\s+/,$opts;
    return @opts;

}

sub new_ext {
    my ($file,$ext) = @_;
    confess "Missing file"  if !$file;

    my ($ext_in) = $file =~ m{.*\.(.*)};
    $ext = "dst.$ext"  if $ext eq $ext_in;
    my $file_out = $file;
    $file_out =~ s/(.*)\.(\w+)$/$1.$ext/;
    if ( -f $file_out ){
        warn "$file_out already exists\n"   if $DEBUG;
        return;
    }
    return $file_out;
}

sub rename_outs {
    my ($file_in, $file_out) = @_;

    my $file_orig = $file_in;
    $file_orig =~ s/(.*)\.($DEFAULT_EXT)/$1.orig.$2/;

    copy($file_in, $file_orig)  or die "$! $file_in -> $file_orig";
    copy($file_out,$file_in  )  or die "$! $file_orig -> $file_in";
    unlink($file_out)           or die "$! $file_out";
}

sub process {
    my $file_in = shift;
    my ($ext_out) = (shift or $DEFAULT_EXT);

    my ($ext_in) = $file_in =~ m{.*\.(.*)};
    $ext_in = lc($ext_in);

    my $file_out = new_ext($file_in, $ext_out)       or return;
    my $file_err = new_ext($file_in,"$ext_out.err")  or return;

    if ($ext_in =~ /^webm$/ && $ext_out eq $DEFAULT_EXT) {
        $file_in = process($file_in,'avi') or return;
    }
    
    my @avconv_opts = split(/ /,$OPTS{$ext_out})    if $OPTS{$ext_out};
    my @cmd = ($AVCONV,"-i",$file_in,@avconv_opts,$file_out);
    warn "@cmd\n"   if $DEBUG;
    print "processing $file_in -> $ext_out\n"   if $VERBOSE;
    my ($in,$out,$err);
    run(\@cmd, \$in, \$out, \$err);
    if ($?) {
        open my $h_err,'>',$file_err or die "$! $file_err";
        print $h_err $err;
        close $h_err;
        warn $err if $VERBOSE;
    } elsif ($ext_in eq $DEFAULT_EXT) {
        rename_outs($file_in,$file_out);
    }
    return $file_out;
}

sub valid_ext {
    my $file = shift;
    my ($ext) = $file =~ m{.*\.(.+)};
    warn "I can't find ext for $file\n"  if !$ext;
    return $VALID_EXT{lc($ext)} if $ext;
}

sub processed_default {
    my $file = shift or die "Missing file to check";
    die "File '$file' is no $DEFAULT_EXT"   if $file !~/$DEFAULT_EXT$/;
    my ($name,$ext) = $file =~ /(.*)\.(.*)/;
    $ext = lc($ext);
    return 1 if $FILES_SRC{"$name.orig.$DEFAULT_EXT"};
    for (keys %VALID_EXT) {
        print "\tfound $name.$_" && return 1 if $_ ne $ext && $FILES_SRC{"$name.$_"};
    }
    return 0;
}

sub processed {
    my $file = shift or die "Missing file to check";
    my $processed = 0;
    $processed = processed_default($file) if $file =~ /$DEFAULT_EXT$/;

    my $file_out = $file;
    if ($file_out =~ /$DEFAULT_EXT$/i) {
        $file_out =~ s/(.*?)\..*/$1.orig.$DEFAULT_EXT/;
    } else {
        $file_out =~ s/(.*?)\..*/$1.$DEFAULT_EXT/;
    }
    $processed = $FILES_SRC{$file_out}
        if !$processed;

    if ($DEBUG) {
        print "$file ";
        print "NOT "    if !$processed;
        print "processed [$file_out]";
        print ".\n";
    }
    return $processed;
}

#######################################################################
#
# main
#
opendir(my $ls, $DIR) or die "$! $DIR\n";
while (my $file = readdir $ls) {
    next if $file !~ /.*\.\w+/;
    $FILES_SRC{$file}++;
}

for my $file (sort keys %FILES_SRC) {
    next if $file =~ /.orig.$DEFAULT_EXT$/i;
    next if processed($file);
    next if !-f "$DIR/$file";
    next if !valid_ext($file);
    my @stat = stat("$DIR/$file") or die "$! $DIR/$file";
    print $file." ".(time - $stat[9])." old\n"    if $DEBUG;
    next if time - $stat[9] < $SECONDS_OLD;
    process("$DIR/$file");
}
closedir $ls;

